import time

from transformers import pipeline, AutoTokenizer, AutoModelForQuestionAnswering

# MODEL_NAME = "twmkn9/albert-base-v2-squad2"
# MODEL_NAME = "ktrapeznikov/albert-xlarge-v2-squad-v2"
# MODEL_NAME = "ahotrod/albert_xxlargev1_squad2_512"
MODEL_NAME = "replydotai/albert-xxlarge-v1-finetuned-squad2"


tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)
model = AutoModelForQuestionAnswering.from_pretrained(MODEL_NAME)

nlp = pipeline(task='question-answering',
               # config="model/config.json",
               model=model,
               tokenizer=tokenizer
               )

s_time = time.time()

context = "An economic system is a means by which societies or governments organize and distribute available " \
          "resources, services, and goods across a geographic region or country. Economic systems regulate " \
          "factors of production, including capital, labor. Socialism is a political, social and economic " \
          "philosophy encompassing a range of economic and social systems characterised by social ownership " \
          "of the means of production and workers' self-management of enterprises. Capitalism is an economic " \
          "system based on the private ownership of the means of production and their operation for profit. " \
          "Characteristics central to capitalism include private property, capital accumulation, wage labor, " \
          "voluntary exchange, a price system and competitive markets. Nationalism is an ideology and movement " \
          "that promotes the interests of a particular nation especially with the aim of gaining and maintaining " \
          "the nation's sovereignty over its homeland. In political science, a political system defines the " \
          "process for making official government decisions. It is usually compared to the legal system, economic " \
          "system, cultural system, and other social systems."

for question in [
    "What is nationalism?",
    "What is socialism?",
    "What is capitalism?",
    "Who is Donald Trump?",
    "Who is Barack Obama?"
]:
    answer = nlp({
        'question': question,
        "context": context
    })
    print(f'\nQuestion: {question}')
    print(f"Answer: {answer}")

print(f"Elapsed: {time.time() - s_time}")
