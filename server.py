from flask import Flask, request, jsonify
from transformers import AutoTokenizer, AutoModelForQuestionAnswering, pipeline


from settings import SERVER_HOST, SERVER_PORT, MODEL_NAME


app = Flask(__name__)


tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)
model = AutoModelForQuestionAnswering.from_pretrained(MODEL_NAME)

nlp = pipeline(task='question-answering', model=model, tokenizer=tokenizer)


def handle_response(body, status_code):
    resp = jsonify(body)
    resp.status_code = status_code
    return resp


@app.route('/api/v1/answer', methods=['POST'])
def success():
    if request.method == 'POST':
        context = request.json.get('context')
        question = request.json.get('question')
        if context and question:
            answer = nlp({
                'question': question,
                "context": context
            })
            return handle_response(answer, 200)
        else:
            return handle_response({"message": "Invalid context or question"}, 403)


if __name__ == '__main__':
    app.run(debug=True, host=SERVER_HOST, port=SERVER_PORT)
