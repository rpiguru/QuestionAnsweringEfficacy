# QuestionAnsweringEfficacy

QuestionAnsweringEfficacy

*NOTE*: Make sure that CUDA 10.1 is installed. 

## Installation

```shell script
    sudo apt install -y python3-dev python3-pip
    sudo pip3 install -U pip
    sudo pip3 install -r requirements.txt
    
```

## Run server
```shell script
sudo python3 server.py
```

## How to replace model

Please open `settings.py` and select a proper model.

```python
MODEL_NAME = "twmkn9/albert-base-v2-squad2"
# MODEL_NAME = "ktrapeznikov/albert-xlarge-v2-squad-v2"
# MODEL_NAME = "ahotrod/albert_xxlargev1_squad2_512"
# MODEL_NAME = "replydotai/albert-xxlarge-v1-finetuned-squad2"
```
And restart your server! :)
